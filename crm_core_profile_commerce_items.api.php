<?php

/**
 * @file
 * Describes hooks provided by CRM Core Profile Commerce Items module.
 */

/**
 * Fires before anything is added to CRM Core Profile generated form.
 *
 * @param array $data
 *   Data to alter.
 * $data = array(
 *   'form' => $form,
 *   'form_state' => $form_state,
 * );
 * @param array $context
 *   CRM Core Profile configuration context.
 * $context = array(
 *   'profile' => $profile,
 * );
 */
function hook_crm_core_profile_commerce_pre_execute_alter(&$data, $context) {}

/**
 * Fires after CRM Core Profile generated form is filled with additional data.
 *
 * @param array $data
 *   Data to alter.
 * $data = array(
 *   'form' => $form,
 *   'form_state' => $form_state,
 * );
 * @param array $context
 *   CRM Core Profile configuration context.
 * $context = array(
 *   'profile' => $profile,
 * );
 */
function hook_crm_core_profile_commerce_post_execute_alter(&$data, $context) {}


/**
 * Allows to alter generated order before total is calculated.
 *
 * Together with the two previous hooks allows to dynamically inject additional
 * options to the profile form to be added to the order.
 *
 * @param object $order
 *   Commerce order object.
 * @param array $context
 *   CRM Core Profile configuration context.
 * $context = array(
 *   'profile' => $profile,
 *   'form' => $form,
 *   'form_state' => $form_state,
 * );
 *
 * @see hook_crm_core_profile_commerce_pre_execute_alter()
 * @see hook_crm_core_profile_commerce_post_execute_alter()
 */
function hook_crm_core_profile_commerce_pre_order_total_calculate_alter(&$order, $context) {}
